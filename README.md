# PicoSID

[![License: CERN-OHL-S-2.0](./images/licence-cern-ohl-s-2.0.svg)](https://codeberg.org/CBMretro/PicoSID/src/branch/main/LICENSE)

<img src="./images/PicoSID.png" width="800" />

---

This is a hardware design concept for PicoSID. A Commodore 64 SID sound chip replacement using Raspberry Pi RP2040 microcontroller.

RP2040 is a modern 32-bit microcontroller with 133 MHz dual ARM Cortex-M0+ cores. More accurate SID emulation is possible than with the ancient 8-bit ATMEGA88 used in SwinSID.

RP2040 only costs about 1 € and is widely available.

Hardware design is currently untested and no software exists. I'm not a coder so if this is ever going to be a working SID replacement someone else needs to write the software. Please contribute to the project if you can!

This is not a DIY friendly design. RP2040 is only available in QFN package and resistors and capacitors are 0402 size. However components are chosen from the JLCPCB SMT assembly parts library so assembled boards can be easily ordered. Gerber, BOM and component position files required for JLCPCB SMT assembly are available in Gerber directory. Again no PCBs have been produced yet using these files so working of the design is untested.

PCB design uses VIAs-In-Pad so "Via Covering: Epoxy Filled & Capped" is required when ordering PCBs.

## Schematic

<img src="./images/PicoSID-schematic.png" />

---

RP2040 doesn't have a DAC but can output audio through PWM. Passive filtering is required to filter out unwanted noise from the output. Raspberry Pi 3 uses PWM for it's 3,5mm analog audio out and I've copied the analog output filter from [Raspberry Pi 3 schematic.](https://datasheets.raspberrypi.com/rpi3/raspberry-pi-3-a-plus-reduced-schematics.pdf)

SID I/O is 5V and GPIO in RP2040 is 3.3V so level shifting is required. Two Runic Technology RS0108YQ20 chips handle the level shifting.

Board has 16 MB flash chip (maximum supported by RP2040). RP2040 can boot from flash or USB. For USB booting jumper J2 needs to be closed.

RP2040 has four ADC inputs. POTX and POTY are connected to ADC inputs so C64 paddle support is possible.

## Links

- [RP2040 Datasheet](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf)
- [Hardware Design with RP2040](https://datasheets.raspberrypi.org/rp2040/hardware-design-with-rp2040.pdf)